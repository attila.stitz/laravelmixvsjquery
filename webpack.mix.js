const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.scripts([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/jquery-ui-dist/jquery-ui.min.js',
        'node_modules/datatables.net/js/jquery.dataTables.js',
        'node_modules/datatables.net-jqui/js/dataTables.jqueryui.js',
        'node_modules/sweetalert2/dist/sweetalert2.all.min.js',
        'node_modules/select2/dist/js/select2.full.js',
    ]
    , 'public/js/customize.js')
    .js('resources/js/app.js', 'public/js/app.js')
    .sass('resources/sass/app.scss', 'public/css').version()
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);

/*
    .autoload({
    jquery: ['$', 'window.jQuery']
});
*/


mix.browserSync({
    proxy: 'http://localhost:9000/',
    files: [
        'webpack.mix.js',
        'resources/views/!*.blade.php',
        'resources/views/!**!/!*.blade.php',
        'resources/!**'
    ]
});
